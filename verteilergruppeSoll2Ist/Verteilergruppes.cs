﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Management.Automation;
using System.Management.Automation.Runspaces;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace verteilergruppeSoll2Ist
{
    class Verteilergruppes : List<Verteilergruppe>
    {
        public Verteilergruppes(string path)
        {
            try
            {
                foreach (string line in File.ReadAllLines(path))
                {
                    Verteilergruppe verteilergruppe = new Verteilergruppe();

                    verteilergruppe.Name = line.Split(';')[0];
                    
                    foreach (var leh in (line.Split(';')[1]).Split(','))
                    {
                        verteilergruppe.Lehrers.Add(new Lehrer(leh));
                    }

                    if (verteilergruppe.Lehrers.Count > 0)
                    {
                        this.Add(verteilergruppe);
                    }                    
                }
                Console.WriteLine("[i] " + this.Count + " Soll-Verteilergruppen insgesamt.");                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public Verteilergruppes(Verteilergruppes verteilergruppesSoll)
        {
            try
            {
                Console.Write(("[i] Verbindung zum Exchange wird hergestellt ...").PadRight(76,'.'));
                string strResult = string.Empty;
                RunspaceConfiguration rsConfig = RunspaceConfiguration.Create();
                PSSnapInInfo info = rsConfig.AddPSSnapIn("Microsoft.Exchange.Management.PowerShell.Snapin", out PSSnapInException snapInException);
                Runspace runspace = RunspaceFactory.CreateRunspace(rsConfig);
                runspace.Open();

                Command getDistributionGroup = new Command("Get-DistributionGroup");

                Pipeline commandPipeLine = runspace.CreatePipeline();
                commandPipeLine.Commands.Add(getDistributionGroup);

                Collection<PSObject> dgs = commandPipeLine.Invoke();

                Console.WriteLine(" ok");
                
                Console.WriteLine(administratorIstOwner("Erweiterte-Schulleitung"));

                if (dgs.Count > 0)
                {
                    Console.WriteLine(dgs.Count());

                    foreach (var vgSoll in verteilergruppesSoll)
                    {
                        // Wenn eine Verteilergruppe noch nicht existiert, wird sie angelegt:

                        Console.WriteLine("VGSOLL: " + vgSoll.Name);

                        foreach (var item in dgs)
                        {
                            Console.WriteLine(item.Properties["Name"].Value.ToString());
                            //Console.WriteLine(item.Properties["Description"].Value.ToString());
                            //Console.ReadKey();
                            
                        }

                        if (!(from exchangeDistributionGroup in dgs
                              where exchangeDistributionGroup.Properties["Name"].Value.ToString().ToUpper() == vgSoll.Name.ToUpper()
                              //where exchangeDistributionGroup.Properties["Description"].Value.ToString().ToUpper().Contains("AUTOMATISCH")
                              select exchangeDistributionGroup
                              ).Any()
                              )
                        {
                            Console.WriteLine(("[+] Verteilergruppe " + vgSoll.Name + " wird angelegt ...").PadRight(76,'.'));
                            //VerteilergruppeAnlegen(vgSoll);         
                            //Console.ReadKey();
                        }
                    }
                    
                    foreach (PSObject obj in dgs)
                    {
                        Console.WriteLine(obj.Properties["Name"].Value.ToString());
                        PSObject Managers = (PSObject)obj.Properties["ManagedBy"].Value;
                        if (Managers.ImmediateBaseObject is System.Collections.ArrayList)
                        {
                            foreach (String Manager in (System.Collections.ArrayList)Managers.BaseObject)
                            {
                                Console.WriteLine("Manager:" + Manager);
                            }
                        }
                        Console.WriteLine("ENTER");
                        Console.ReadKey();
                    }

                    

                    foreach (PSObject dg in (from e in dgs
                                             where e.Properties["Name"].Value.ToString().ToLower() == "klassenbuch" || true
                                             select e)
                                             )
                    {

                        
                        Console.ReadKey();
                                
                        

                        // Wenn die existierende Verteilergruppe in den Soll-Verteilergruppen nicht existiert, ...

                        if (!(from v in verteilergruppesSoll
                              where v.Name.ToLower() == dg.Properties["Name"].Value.ToString().ToLower()
                              select v
                              ).Any())
                        {
                            // ... wird sie gelöscht.

                            Console.Write(("[-] Verteilergruppe " + dg.Properties["Name"].Value.ToString() + " ...").PadRight(76, '.'));

                            //Todo VG löschen

                            Console.WriteLine(" ok");                            
                        }   
                        else
                        {   
                            Command getDistributionGroupMember = new Command("Get-DistributionGroupMember");

                            commandPipeLine = runspace.CreatePipeline();
                            commandPipeLine.Commands.Add(getDistributionGroupMember);
                            getDistributionGroupMember.Parameters.Add(new CommandParameter("Identity", dg.Properties["Name"].Value.ToString()));
                            
                            Collection<PSObject> resultsPersonen = commandPipeLine.Invoke();

                            // Für jeden bereits im Exchange existierenden Member wird geprüft, ...

                            if (resultsPersonen.Count > 0)
                            {
                                foreach (PSObject resultPerson in resultsPersonen)
                                {
                                    // ... ob der Member noch immer Member der Exchange-Verteilergruppe ist. Gegebenenfalls ...

                                    if (!(from v in verteilergruppesSoll
                                          from lee in v.Lehrers
                                          where v.Name.ToLower() == dg.Properties["Name"].Value.ToString().ToLower()
                                          where lee.Alias.ToLower() == resultPerson.Properties["Alias"].Value.ToString().ToLower()
                                          select v
                                          ).Any())
                                    {
                                        // ... wird er aus der Verteilergruppe gelöscht.

                                        Console.Write(("[-] " + resultPerson.Properties["Alias"].Value.ToString().ToLower() + " wird aus " + dg.Properties["Name"].Value.ToString() + " gelöscht ...").PadRight(76, '.'));

                                        //Todo: Alias löschen

                                        Console.WriteLine(" ok");
                                    }
                                }
                            }

                            // Wenn Personen fehlen ...
                            
                            foreach (var l in (from v in verteilergruppesSoll
                                               where v.Name.ToLower() == dg.Properties["Name"].Value.ToString().ToLower()
                                               select v.Lehrers
                                               ).FirstOrDefault())
                            {
                                if (!(from resultPerson in resultsPersonen
                                      where resultPerson.Properties["Alias"].Value.ToString().ToLower() == l.Alias.ToLower()
                                      select resultPerson
                                      ).Any())
                                {
                                    // ... werden sie angelegt.

                                    Console.Write(("[+] " + l.Alias + " wird zu " + dg.Properties["Name"].Value.ToString() + " hinzugefügt ...").PadRight(76,'.'));

                                    //Todo Person hinzufügen

                                    Console.WriteLine(" ok");
                                }
                            }
                            Console.WriteLine("Enter");
                            Console.ReadKey();
                        }                        
                    }
                }
                runspace.Dispose();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Console.ReadKey();
            }
        }

        private bool administratorIstOwner(string exchangeDistributionGroupName)
        {
            RunspaceConfiguration runspaceConfiguration = RunspaceConfiguration.Create();
            PSSnapInException psSnapInException = null;
            PSSnapInInfo pSSnapInInfo = runspaceConfiguration.AddPSSnapIn("Microsoft.Exchange.Management.PowerShell.Snapin", out psSnapInException);
            Runspace runspace = RunspaceFactory.CreateRunspace(runspaceConfiguration);
            runspace.Open();

            Pipeline pipeline = runspace.CreatePipeline();
            Command getDistributiongroup = new Command("Get-Distributiongroup");
            getDistributiongroup.Parameters.Add("Identity", exchangeDistributionGroupName);
            pipeline.Commands.Add(getDistributiongroup);
            Command select = new Command("select");
            select.Parameters.Add("first", 3);
            pipeline.Commands.Add(select);

            Collection<PSObject> results = pipeline.Invoke();

            foreach (var item in results)
            {                
                Console.WriteLine("item: " + item);
                Console.ReadKey();
            }

            return false;
        }

        private void VerteilergruppeAnlegen(Verteilergruppe vgSoll)
        {
            string command = "New-DistributionGroup";

            List<CommandParameter> commandparameters = new List<CommandParameter>();
            commandparameters.Add(new CommandParameter("Name", vgSoll.Name));
            commandparameters.Add(new CommandParameter("OrganizationalUnit", "bkb.local/BKB/Gruppen/Verteilergruppen"));
            commandparameters.Add(new CommandParameter("SAMAccountName", vgSoll.Name));
            commandparameters.Add(new CommandParameter("Type", "Distribution"));
            commandparameters.Add(new CommandParameter("Notes", SignaturHinzufügen()));            
            InvokePowershell(command, commandparameters);
        }

        private void DialogRendern(Func<object> p)
        {
            throw new NotImplementedException();
        }

        private void InvokePowershell(string befehl, List<CommandParameter> commandparameters)
        {
            RunspaceConfiguration runspaceConfiguration = RunspaceConfiguration.Create();
            PSSnapInException psSnapInException = null;
            PSSnapInInfo pSSnapInInfo = runspaceConfiguration.AddPSSnapIn("Microsoft.Exchange.Management.PowerShell.Snapin", out psSnapInException);
            Runspace runspace = RunspaceFactory.CreateRunspace(runspaceConfiguration);
            runspace.Open();

            Command command = new Command(befehl);

            foreach (CommandParameter commandParameter in commandparameters)
            {
                command.Parameters.Add(commandParameter);
            }

            Pipeline pipeLine = runspace.CreatePipeline();
            pipeLine.Commands.Add(command);

            Collection<PSObject> results = pipeLine.Invoke();
                        
            string fehler = pipeLine.Error.Read().ToString();

            if (fehler.Length > 0)
            {
                Console.WriteLine("...");
                Console.WriteLine(fehler.ToString());
            }
            else
            {
                Console.WriteLine(" ok");
            }
            runspace.Dispose();
        }

        private object SignaturHinzufügen()
        {
            return "automatisch erstellt | " + System.Environment.UserName + " | " + DateTime.Now;
        }
    }
}