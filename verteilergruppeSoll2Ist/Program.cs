﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace verteilergruppeSoll2Ist
{
    class Program
    {
        static void Main(string[] args)
        {
            Lehrers lehrers = new Lehrers();
                        
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\verteilergruppen.txt";            

            Verteilergruppes verteilergruppesSoll = new Verteilergruppes(path);

            Verteilergruppes verteilergruppes = new Verteilergruppes(verteilergruppesSoll);
            Console.ReadKey();
        }
    }
}
