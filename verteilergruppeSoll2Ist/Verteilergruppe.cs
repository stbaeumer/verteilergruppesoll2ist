﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace verteilergruppeSoll2Ist
{
    class Verteilergruppe
    {
        public string Name { get; internal set; }
        public Lehrers Lehrers { get; internal set; }

        public Verteilergruppe()
        {
            Lehrers = new Lehrers();
        }

        public Verteilergruppe(string name, Lehrers lehrers)
        {
            this.Name = name;
            Lehrers = Lehrers;
            //Lehrers = lehrers;
        }
    }
}
